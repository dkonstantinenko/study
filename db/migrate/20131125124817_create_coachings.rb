class CreateCoachings < ActiveRecord::Migration
  def change
    create_table :coachings do |t|
      t.integer :trainer_id
      t.integer :training_id
      t.datetime :coaching_date

      t.timestamps
    end
  end
end
