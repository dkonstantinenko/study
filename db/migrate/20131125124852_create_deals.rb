class CreateDeals < ActiveRecord::Migration
  def change
    create_table :deals do |t|
      t.integer :company_id
      t.integer :client_id
      t.datetime :deal_date

      t.timestamps
    end
  end
end
