class CreateTrainers < ActiveRecord::Migration
  def change
    create_table :trainers do |t|
      t.string :name
      t.string :telephone
      t.string :email
      t.string :detail_info
      t.integer :company_id

      t.timestamps
    end
  end
end
