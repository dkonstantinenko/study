class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.integer :town_id
      t.integer :training_id
      t.datetime :place_date

      t.timestamps
    end
  end
end
