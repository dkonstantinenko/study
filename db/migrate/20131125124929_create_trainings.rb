class CreateTrainings < ActiveRecord::Migration
  def change
    create_table :trainings do |t|
      t.string :name
      t.date :date
      t.boolean :online
      t.integer :price
      t.integer :category_id
      t.integer :town_id
      t.integer :company_id
      t.integer :trainer_id

      t.timestamps
    end
  end
end
