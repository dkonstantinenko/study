class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :telephone
      t.string :email
      t.string :office_address
      t.string :contact_person
      t.integer :client_id

      t.timestamps
    end
  end
end
