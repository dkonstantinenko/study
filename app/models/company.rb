class Company < ActiveRecord::Base
  attr_accessible :client_id, :contact_person, :email, :name, :office_address, :telephone
end
