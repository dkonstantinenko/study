class Trainer < ActiveRecord::Base
  attr_accessible :company_id, :detail_info, :email, :name, :telephone
end
