class CoachingsController < ApplicationController
  # GET /coachings
  # GET /coachings.json
  def index
    @coachings = Coaching.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @coachings }
    end
  end

  # GET /coachings/1
  # GET /coachings/1.json
  def show
    @coaching = Coaching.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @coaching }
    end
  end

  # GET /coachings/new
  # GET /coachings/new.json
  def new
    @coaching = Coaching.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @coaching }
    end
  end

  # GET /coachings/1/edit
  def edit
    @coaching = Coaching.find(params[:id])
  end

  # POST /coachings
  # POST /coachings.json
  def create
    @coaching = Coaching.new(params[:coaching])

    respond_to do |format|
      if @coaching.save
        format.html { redirect_to @coaching, notice: 'Coaching was successfully created.' }
        format.json { render json: @coaching, status: :created, location: @coaching }
      else
        format.html { render action: "new" }
        format.json { render json: @coaching.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /coachings/1
  # PUT /coachings/1.json
  def update
    @coaching = Coaching.find(params[:id])

    respond_to do |format|
      if @coaching.update_attributes(params[:coaching])
        format.html { redirect_to @coaching, notice: 'Coaching was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @coaching.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coachings/1
  # DELETE /coachings/1.json
  def destroy
    @coaching = Coaching.find(params[:id])
    @coaching.destroy

    respond_to do |format|
      format.html { redirect_to coachings_url }
      format.json { head :no_content }
    end
  end
end
